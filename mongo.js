//Inside of b126



/*
1. Create a collection named channels

2. inside the channels collection, creat 5 different documents each with the following information:

-"channelName": ("Red Team", "Blue Team", "Yellow Team", "Black Team", "White Team")

3. Change the Yellow and Black Team to inactive

4. Remove the White Team Channel

5. Find all active channels

6. Delete all inactive channels


*/


use b126_chatapp


db.b126_chatapp.insertMany([
	{
		"channelName" : "Red Team",
		"members":[
		{
			"member1" : "Ned",
			"member2" : "Caitlyn",
			"member3" : "Bran"
		}]
	},
		
	{
		"channelName" : "Blue Team",
		"members":[
		{
			"member1" : "Jon",
			"member2" : "Arya",
			"member3" : "Rickon"
		}]
	},
	{
		"channelName" : "Yellow Team",
		"members":[
		{
			"member1" : "Cersei",
			"member2" : "Jeofrrey",
			"member3" : "Jaime"
		}]
	},
	{
		"channelName" : "Black Team",
		"members":[
		{
			"member1" : "Daenery",
			"member2" : "Missandei",
			"member3" : "Jorah"
		}]
	},
	{
		"channelName" : "White Team", 
		"members":[
		{
			"member1" : "Petyr",
			"member2" : "Tyrion",
			"member3" : "Varys"
		}]
	}
])



db.b126_chatapp.updateMany(
	{},
	{
		$set : {
			"isActive":true
		}
	}

)





db.b126_chatapp.updateMany(
	{
		"_id" : ObjectId("6172af5896640a83b6a2ac1e"),
		"_id" : ObjectId("6172af5896640a83b6a2ac1f")
	},

	{
		$set : {
			"isActive" :false
		}
	}

)

db.b126_chatapp.deleteOne(
	{
		"_id" : ObjectId("6172af5896640a83b6a2ac20")
	}
)

db.b126_chatapp.find({"isActive" : true})


db.b126_chatapp.deleteMany(
	{
		"isActive":false
	}

)